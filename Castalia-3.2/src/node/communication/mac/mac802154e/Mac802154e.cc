//****************************************************************************
//*                                                                          *
//* brief: The defination of ieee802154ee (TSCH)                              *
//* This file is distributed under the terms in the attached LICENSE file.   *
//* Author: tengfei.chang, tengfei.chang@gmail.com                           *
//*                                                                          *  
//****************************************************************************/

#include "Mac802154e.h"

Define_Module(Mac802154e);

void Mac802154e::startup()
{
    slotframeLength     = par("slotframeLength");        
    EBPeriod            = par("EBPeriod");               
    maxEBDelay          = par("maxEBDelay");             
    numNeighboursTOWait = par("numNeighboursTOWait");
    isDAGroot           = par("isDAGroot");

	/**************************************************************************************************
	 *			802.15.4e specific intialize
	 **************************************************************************************************/
    // initial asn
    for (int i=0;i<3;i++) {
    	asn[i] = 0;
    }
    slotOffset = 0;
    asnOffset  = 0;

	packetBreak.clear();
	fsm_state = S_SLEEP;

	if (isDAGroot) {
		isSync = true;
	} else {
		isSync = false;
	}
    // minimal configuration. One active slot.
    addActiveSlot(0,   // slotOffset
                  0,   // channelOffset
                  CELLTYPE_ADV | CELLTYPE_TX | CELLTYPE_TX | CELLTYPE_SHARED, // type
                  true,// shared? 
                  0);  // if adv or shared, the neighbor item will be invalue. We just set it to 0
    nextActiveSlot = schedule.begin();

    setTimer(SLOT_DURATION, 0);	//slot communication start.
    setTimer(EB_TIMEOUT,10);    //schedule a beacon packet at 10 second
}

void Mac802154e::timerFiredCallback(int index)
{
	switch (index) {
		  case EB_TIMEOUT:
          advPacket = new Mac802154eBeaconPacket("ADV packet",MAC_LAYER_PACKET);
          toRadioLayer(advPacket);
			    break;
		  case SLOT_DURATION:
    	    setTimer(SLOT_DURATION, macTsTimeslotLength);
    	    if (isSync == false) {
    	      	if (isDAGroot) {
    	    	  	  isSync = true;
    	    	  } else {
    	    		    activity_synchronize_newSlot();
    	    	  }
    	      } else {
    	    	  activity_ti1ORri1();
    	      }
    	    break;
      case SLOT_FSM:
		      switch (fsm_state) {
              case S_TXDATAOFFSET:
                    fsm_state = S_TXDATAPREPARE;
                    outputFSMState(fsm_state);
                    toRadioLayer(createRadioCommand(SET_CARRIER_FREQ,SYNCHRONIZING_CHANNEL));
                    // load packet
                    // enable tx
                    setTimer(SLOT_FSM,macTsCCA);
                    fsm_state = S_TXDATAREADY;
                    outputFSMState(fsm_state);
                    break;
              case S_TXDATAREADY:
                    fsm_state = S_TXDATADELAY;
                    outputFSMState(fsm_state);
                    toRadioLayer(createRadioCommand(SET_STATE,TX));
                    break;
              case S_RXACKOFFSET:
                    fsm_state = S_RXACKPREPARE;
                    outputFSMState(fsm_state);
                    toRadioLayer(createRadioCommand(SET_CARRIER_FREQ,SYNCHRONIZING_CHANNEL));
                    // radio rx
                    toRadioLayer(createRadioCommand(SET_STATE,RX));
                    setTimer(SLOT_FSM,macTsAckWait);
                    fsm_state = S_RXACKREADY;
                    outputFSMState(fsm_state);
                    break;
		    }
			break;
	}
}

/* This indicates that a packet is received from upper layer (Network)
 * First we check that the MAC buffer is capable of holding the new packet, also check
 * that length does not exceed the valid mac frame size. Then store a copy of the packet
 * is stored in transmission buffer. We dont need to encapsulate the message here - it will
 * be done separately for each transmission attempt
 */
void Mac802154e::fromNetworkLayer(cPacket * pkt, int dstMacAddress)
{
	Mac802154ePacket *macPacket = new Mac802154ePacket("802.15.4e MAC data packet", MAC_LAYER_PACKET);
	macPacket->setSrcID(SELF_MAC_ADDRESS);	//if we are connected, we would have short MAC address assigned, 
	//but we are not using short addresses in this model
	macPacket->setDstID(dstMacAddress);
	macPacket->setMac802154ePacketType(MAC_802154E_DATA_PACKET);
	encapsulatePacket(macPacket, pkt);
}

void Mac802154e::finishSpecific()
{

}

/* This function will handle a MAC frame received from the lower layer (physical or radio)
 */
void Mac802154e::fromRadioLayer(cPacket * pkt, double rssi, double lqi)
{
	  Mac802154ePacket *rcvPacket = dynamic_cast < Mac802154ePacket * >(pkt);
	  if (!rcvPacket)
		    return;

	  switch (rcvPacket->getMac802154ePacketType()) {

		/* received a BEACON frame */
		case MAC_802154E_BEACON_PACKET:
        trace() << "Get one BEeacon Packet";
        if (isSync) {
            // record the information
        } else {
            if (fsm_state != S_SYNCRX) {
              return ;
            }
            fsm_state = S_SYNCPROC;
            synchronizePacket(lastCapturedTime);
        }
			  break;

		// data frame
		case MAC_802154E_DATA_PACKET:{
        trace() << "Get one Data Packet";
			  break;
		}

		//
		case MAC_802154E_ACK_PACKET:
        trace() << "Get one Ack packet";
			  break;

		// 
		case MAC_802154E_CMD_PACKET:
        trace() << "Get one CMD Packet";
			  break;

		default:{
			  trace() << "WARNING: unknown packet type received " << rcvPacket->getMac802154ePacketType() << 
					  " [" << rcvPacket->getName() << "]";
		}
	}
}

int Mac802154e::handleRadioControlMessage(RadioControlMessage *radioMsg){
  trace() <<"Mac802154e Mac ";
  switch (radioMsg->getRadioControlMessageKind()) {
    case RADIO_STARTOFFRAME_INTERRUPT:
      trace() << "Start of Frame at: "<< getTimer(SLOT_DURATION);
      if (isSync == false) {
        activity_synchronize_startOfFrame(getTimer(SLOT_DURATION)); // this vaule may not be right
      } else {
        switch(fsm_state) {
          case S_TXDATADELAY:
            activity_ti4(getTimer(SLOT_DURATION));
            break;
          case S_RXACKREADY:
            activity_ti8(getTimer(SLOT_DURATION));
            break;
          case S_RXACKLISTEN:
            break;
          case S_RXDATAREADY:
            break;
          case S_RXDATALISTEN:
            break;
          case S_TXACKDELAY:
            break;
          default:
            trace() << "Wrong FSM state, log this message";
        }
      }
      break;
    case RADIO_ENDOFFRAME_INTERRUPT:
      trace() << "End of Frame at: "<< getTimer(SLOT_DURATION);
      switch(fsm_state) {
        case S_TXDATA:
          activity_ti5(getTimer(SLOT_DURATION));
          break;
      }
      break;
    default:
      toNetworkLayer(radioMsg);
  }
  return 1;
}

/**
\brief this function will be called when the new slot start and I am not synchronized yet.
*/
void Mac802154e::activity_synchronize_newSlot()
{
   if (fsm_state == S_SYNCRX) {
	return;
   }

   // if this is the first time I call this function while not synchronized,
   // switch on the radio in Rx mode
   if (fsm_state != S_SYNCLISTEN) {
      // change state
      fsm_state = S_SYNCLISTEN;
      
      // turn off the radio (in case it wasn't yet)
      toRadioLayer(createRadioCommand(SET_STATE,SLEEP));
      
      // configure the radio to listen to the default synchronizing channel
      toRadioLayer(createRadioCommand(SET_CARRIER_FREQ,SYNCHRONIZING_CHANNEL));
      
      // switch on the radio in Rx mode.
      toRadioLayer(createRadioCommand(SET_STATE,RX));
   }
   
}

void Mac802154e::activity_synchronize_startOfFrame(simtime_t captureTime)
{
    if (fsm_state!=S_SYNCLISTEN) {
      return;
    }

    fsm_state = S_SYNCRX;
    lastCapturedTime = captureTime;

}

/**
\brief this function will be called when new slot start and I am sync-ed.
*/
void Mac802154e::activity_ti1ORri1()
{
    bool hasPacketToBeSent = false;
    
    incrementAsnOffset();
    trace() << "ASN        = "<<asn[2]*65536*65536<<":"<<asn[1]*65536<<":"<<asn[0];
    trace() << "SlotOffset = "<<slotOffset;

    if(slotOffset == nextActiveSlot->slotOffset) {
        nextActiveSlot++;
        if(nextActiveSlot == schedule.end()) {
            nextActiveSlot = schedule.begin();        
        }

        if(nextActiveSlot->type & CELLTYPE_ADV) {
            hasPacketToBeSent = checkADVPacket();
            if(hasPacketToBeSent) {
                setTimer(SLOT_FSM, macTsCCAOffset);	//slot communication start.
                fsm_state = S_TXDATAOFFSET;
            }
        }
        if(hasPacketToBeSent != true && nextActiveSlot->type & CELLTYPE_SHARED) {
            hasPacketToBeSent = checkSharedPacket();      
        }
        if(hasPacketToBeSent != true && nextActiveSlot->type & CELLTYPE_TX) {
            hasPacketToBeSent = checkTxPacket();      
        }
        if(hasPacketToBeSent != true && nextActiveSlot->type & CELLTYPE_RX) {
        }
        
    } else {
        trace() << "Current Slot is OFF type";
    }
}

void Mac802154e::activity_ti4(simtime_t captureTime)
{
  fsm_state = S_TXDATA;
  lastCapturedTime = captureTime; 

  //setTimer(macTsMaxTx);   set a timeout for transmitting packet. we needn't do this in simulator
}

void Mac802154e::activity_ti5(simtime_t captureTime)
{
  bool listenForACK;
  fsm_state = S_RXACKOFFSET;
  // turn off radio 
  toRadioLayer(createRadioCommand(SET_STATE,SLEEP));
  lastCapturedTime = captureTime;

  if(isBoardCastPacket()){
    listenForACK = false;
  } else {
    listenForACK = true;
  }

  if(listenForACK){
    setTimer(SLOT_FSM,macTsRxAckDelay);
  } else {
    // record the result: such as numTx ++
  }
}

void Mac802154e::activity_ti8(simtime_t captureTime)
{
    fsm_state = S_RXACK;
    cancelTimer(SLOT_FSM);
    lastCapturedTime = captureTime;
}

// add active slot to schedule
void Mac802154e::addActiveSlot(int slotOffset, 
                               int channelOffset, 
                               unsigned int type, 
                               bool shared, 
                               int neighbor)
{
    scheduleEntry_t tempSchedule;

    tempSchedule.slotOffset      = slotOffset;
    tempSchedule.channelOffset   = channelOffset;
    tempSchedule.type            = type;
    tempSchedule.shared          = shared;
    tempSchedule.neighborAddress = neighbor;
    tempSchedule.numRx           = 0;
    tempSchedule.numTx           = 0;
    tempSchedule.numTxACK        = 0;

    schedule.push_back(tempSchedule);
}
// remove active slot from scheudle
void Mac802154e::removeActiveSlot(int slotOffset, int neighbor)
{

}

//handling the different type of slot
bool Mac802154e::checkADVPacket() 
{
    trace() << "Current Slot can be an ADV slot";
    return true;
}

bool Mac802154e::checkSharedPacket()
{   
    trace() << "Current Slot can be a Shared slot"; 
    return false;
}

bool Mac802154e::checkTxPacket()
{
    trace() << "Current Slot can be a Tx slot"; 
    return false;
}

/**
\brief  private function. ASN handling.
*/
void Mac802154e::incrementAsnOffset()
{
   // increment the asn
   asn[0]++;
   if (asn[0]==0) {
      asn[1]++;
      if (asn[1]==0) {
         asn[2]++;
      }
   }
   // increment the offsets
   slotOffset  = (slotOffset+1) % slotframeLength;
   asnOffset   = (asnOffset+1)  % 16;
}

int Mac802154e::calculateFrequency()
{
   return 20; 
}

bool Mac802154e::isBoardCastPacket()
{
  // test
  return true;
}

void Mac802154e::outputFSMState(ieee154e_state_t fsm_state)
{
  switch(fsm_state){
      case 0x00: 
        trace() << "Current FSM state is S_SLEEP"; 
        break;
      case 0x01:
        trace() << "Current FSM state is S_SYNCLISTEN";
        break;
      case 0x02:
        trace() << "Current FSM state is S_SYNCRX";
        break;
      case 0x03:
        trace() << "Current FSM state is S_SYNCPROC";
        break;
      // TX
      case 0x04:
        trace() << "Current FSM state is S_TXDATAOFFSET";
        break;
      case 0x05:
        trace() << "Current FSM state is S_TXDATAPREPARE";
        break;
      case 0x06:
        trace() << "Current FSM state is S_TXDATAREADY";
        break;
      case 0x07:  
        trace() << "Current FSM state is S_TXDATADELAY";
        break;
      case 0x08:  
        trace() << "Current FSM state is S_TXDATA";
        break;
      case 0x09:
        trace() << "Current FSM state is S_RXACKOFFSET";
        break;
      case 0x0a:
        trace() << "Current FSM state is S_RXACKPREPARE";
        break;
      case 0x0b:
        trace() << "Current FSM state is S_RXACKREADY";
        break;
      case 0x0c:
        trace() << "Current FSM state is S_RXACKLISTEN";
        break;
      case 0x0d:
        trace() << "Current FSM state is S_RXACK";
        break;
      case 0x0e:
        trace() << "Current FSM state is S_TXPROC";
        break;
        //RX
      case 0x0f:
        trace() << "Current FSM state is S_RXDATAOFFSET";
        break;
      case 0x10:
        trace() << "Current FSM state is S_RXDATAPREPARE";
        break;
      case 0x11:
        trace() << "Current FSM state is S_RXDATAREADY";
        break;
      case 0x12:
        trace() << "Current FSM state is S_RXDATADELAY";
        break;
      case 0x13:
        trace() << "Current FSM state is S_RXDATA";
        break;
      case 0x14:
        trace() << "Current FSM state is S_TXACKOFFSET";
        break;
      case 0x15:
        trace() << "Current FSM state is S_TXACKPREPARE";
        break;
      case 0x16:
        trace() << "Current FSM state is S_TXACKREADY";
        break;
      case 0x17:
        trace() << "Current FSM state is S_TXACKDELAY";
        break;
      case 0x18:
        trace() << "Current FSM state is S_TXACK";
        break;
      case 0x19:
        trace() << "Current FSM state is S_RXPROC";
        break;
      default:
        trace() << "Wrong state.";
  }
}

void Mac802154e::synchronizePacket(simtime_t captureTime){
  simtime_t timeCorrection = captureTime - macTsTxOffset;
  simtime_t newDuration = macTsTimeslotLength + timeCorrection;
  cancelTimer(SLOT_DURATION);
  setTimer(SLOT_DURATION,newDuration-captureTime);
  isSync = true;
}