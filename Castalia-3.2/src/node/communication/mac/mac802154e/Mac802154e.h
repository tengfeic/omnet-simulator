//****************************************************************************
//*                                                                          *
//* brief: The defination of ieee802154e (TSCH)                              *
//* This file is distributed under the terms in the attached LICENSE file.   *
//* Author: tengfei.chang, tengfei.chang@gmail.com                           *
//*                                                                          *  
//****************************************************************************/

#ifndef MAC_802154E_H_
#define MAC_802154E_H_

#include <map>
#include <vector>

#include "VirtualMac.h"
#include "Mac802154ePacket_m.h"

// timeslot timing, in second. See ieee802154e standard, section 6.4.3.3.3
#define macTsCCAOffset       0.001800 
#define macTsCCA             0.000128
#define macTsTxOffset        0.002120
#define macTsRxOffset        0.001120

#define macTsRxAckDelay      0.000800
#define macTsTxAckDelay      0.001000
#define macTsRxWait          0.002200
#define macTsAckWait         0.000400
#define macTsRxTx            0.000192
#define macTsMaxAck          0.002400
#define macTsMaxTx           0.004256
#define macTsTimeslotLength  0.010000 

// setting
#define SYNCHRONIZING_CHANNEL   2400.0
/**********************************************************************
 *
 *********************************************************************/

using namespace std;

// the state of ieee802154e, see openwsn implementation at: openwsn.org
enum ieee154e_state_t{
   S_SLEEP                   = 0x00,   // ready for next slot
   // synchronizing
   S_SYNCLISTEN              = 0x01,   // listened for packet to synchronize to network
   S_SYNCRX                  = 0x02,   // receiving packet to synchronize to network
   S_SYNCPROC                = 0x03,   // processing packet just received
   // TX
   S_TXDATAOFFSET            = 0x04,   // waiting to prepare for Tx data
   S_TXDATAPREPARE           = 0x05,   // preparing for Tx data
   S_TXDATAREADY             = 0x06,   // ready to Tx data, waiting for 'go'
   S_TXDATADELAY             = 0x07,   // 'go' signal given, waiting for SFD Tx data
   S_TXDATA                  = 0x08,   // Tx data SFD received, sending bytes
   S_RXACKOFFSET             = 0x09,   // Tx data done, waiting to prepare for Rx ACK
   S_RXACKPREPARE            = 0x0a,   // preparing for Rx ACK
   S_RXACKREADY              = 0x0b,   // ready to Rx ACK, waiting for 'go'
   S_RXACKLISTEN             = 0x0c,   // idle listening for ACK
   S_RXACK                   = 0x0d,   // Rx ACK SFD received, receiving bytes
   S_TXPROC                  = 0x0e,   // processing sent data
   // RX
   S_RXDATAOFFSET            = 0x0f,   // waiting to prepare for Rx data
   S_RXDATAPREPARE           = 0x10,   // preparing for Rx data
   S_RXDATAREADY             = 0x11,   // ready to Rx data, waiting for 'go'
   S_RXDATALISTEN            = 0x12,   // idle listening for data
   S_RXDATA                  = 0x13,   // data SFD received, receiving more bytes
   S_TXACKOFFSET             = 0x14,   // waiting to prepare for Tx ACK
   S_TXACKPREPARE            = 0x15,   // preparing for Tx ACK
   S_TXACKREADY              = 0x16,   // Tx ACK ready, waiting for 'go'
   S_TXACKDELAY              = 0x17,   // 'go' signal given, waiting for SFD Tx ACK
   S_TXACK                   = 0x18,   // Tx ACK SFD received, sending bytes
   S_RXPROC                  = 0x19,   // processing received data
};

enum Mac802154eTimers {
    EB_TIMEOUT    = 1,
    SLOT_FSM      = 2,
    SLOT_DURATION = 3, 
};

enum cellType_t{
   CELLTYPE_OFF              = 0x00,
   CELLTYPE_ADV              = 0x01,
   CELLTYPE_TX               = 0x02,
   CELLTYPE_RX               = 0x04,
   CELLTYPE_SHARED           = 0x08,
};

struct scheduleEntry_t{
   int          slotOffset;
   unsigned int type;
   bool         shared;
   int          channelOffset;
   int          neighborAddress;
   // used for statistic for the performance of scheduled slot
   int          numRx;
   int          numTx;
   int          numTxACK;
};

struct neighborEntry_t{
   bool         used;
   bool         isTimeSource; 
   int          address;        // use index as id 
   int          DAGrank;
   int          rssi;           // connectivity statistic. Can be replaced by ETX, PDR, LQI, etc
   int          numRx;
   int          numTx;
   int          numTxACK;
   unsigned int asn[3];         // timestamp for last time synchronized
   int          joinPrio;
};

class Mac802154e: public VirtualMac {
 private:
    /*--- A map from int value of state to its description (used in debug) ---*/
    map<int,string> stateDescr;

    /*--- A map for packet breakdown statistics ---*/
    map<string,int> packetBreak;

    /*--- The .ned file's parameters ---*/
    int slotframeLength;        // This value depends on the traffic, delay, energy strategy of a network.
    int EBPeriod;               // in seconds
    int maxEBDelay;             // in seconds
    int numNeighboursTOWait;
    bool isDAGroot;


    /*--- General MAC variable ---*/
    unsigned int asn[3];
    int slotOffset;
    int asnOffset;

    /*--- 802154eMac state variables  ---*/
    map<int,bool> associatedDevices;    // map of assoicated devices (for PAN coordinator)
    bool isSync;
    ieee154e_state_t fsm_state;
    simtime_t lastCapturedTime;

    /*--- 802154eMac packet pointers (sometimes packet is created not immediately before sending) ---*/
    Mac802154eBeaconPacket *advPacket;
    Mac802154eDataPacket   *kaPacket;

    Mac802154ePacket *dataToSent;
    Mac802154ePacket *dataReceived;

    /*--- 802154eMac schedule list --- */
    list<scheduleEntry_t> schedule;
    list<scheduleEntry_t>::iterator nextActiveSlot;

 protected:
    virtual void startup();
    void fromNetworkLayer(cPacket *, int);
    void fromRadioLayer(cPacket *, double, double);

    virtual void finishSpecific();
    void timerFiredCallback(int);
    int handleRadioControlMessage(RadioControlMessage *);

    // for schedule
    void addActiveSlot(int slotOffset, int channelOffset, unsigned int type, bool shared, int neighbor);
    void removeActiveSlot(int slotOffset, int neighbor);
 private:
    // for joinning the network
    void activity_synchronize_newSlot();
    void activity_synchronize_startOfFrame(simtime_t captureTime);

    // for fsm 
    void activity_ti1ORri1();
    void activity_ti4(simtime_t captureTime);
    void activity_ti5(simtime_t);
    void activity_ti8(simtime_t);

    //handling the different type of slot
    bool checkADVPacket();
    bool checkSharedPacket();
    bool checkTxPacket();
    
    // pricate functions
    void incrementAsnOffset();
    int calculateFrequency();
    bool isBoardCastPacket();
    void outputFSMState(ieee154e_state_t);
    void synchronizePacket(simtime_t);
};

#endif                //MAC_802154E_MODULE
